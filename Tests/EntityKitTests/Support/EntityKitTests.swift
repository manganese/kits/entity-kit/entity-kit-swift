import XCTest
@testable import EntityKit

class EntityKitTests: XCTestCase {
  static var WIDGETS_ENTITY_TYPE = "widgets"

  static var WIDGET1 = Widget(id: 1, name: "widget1")
  static var WIDGET2 = Widget(id: 2, name: "widget2")
}
