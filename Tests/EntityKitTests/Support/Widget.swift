import SwiftyJSON

@testable import EntityKit


class Widget: Entity, EncodableEntity, DecodableEntity, Equatable {
  typealias EntityType = Widget

  var id: UInt
  var name: String

  init(id: UInt, name: String) {
    self.id = id
    self.name = name
  }

  static func decode(json: JSON) -> Entity {
    return Widget(
      id: UInt(json["id"].intValue),
      name: json["name"].stringValue
    )
  }

  func encode() -> JSON {
    return [
      "id": self.id,
      "name": self.name
    ]
  }

  static func == (widget1: Widget, widget2: Widget) -> Bool {
    return (
      widget1.id == widget2.id &&
      widget1.name == widget2.name
    )
  }
}
