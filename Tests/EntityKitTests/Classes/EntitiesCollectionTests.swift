import XCTest
import SwiftyJSON

@testable import EntityKit


final class EntitiesCollectionTests: XCTestCase {
  func testInitFrom() {
    let originalEntitiesCollection = EntitiesCollection()
    originalEntitiesCollection.unsafePutEntity(entityType: EntityKitTests.WIDGETS_ENTITY_TYPE, entity: EntityKitTests.WIDGET1)

    let entitiesCollection = EntitiesCollection(from: originalEntitiesCollection)
    let entity = entitiesCollection.getEntity(entityType:EntityKitTests.WIDGETS_ENTITY_TYPE, id: EntityKitTests.WIDGET1.id) as? Widget

    XCTAssertEqual(originalEntitiesCollection.count, entitiesCollection.count)
    XCTAssertEqual(
      entity,
      EntityKitTests.WIDGET1
    )
  }

  func testEncode() {
    do {
      let widget = EntityKitTests.WIDGET1
      let entitiesCollection = EntitiesCollection()
      entitiesCollection.unsafePutEntity(entityType: EntityKitTests.WIDGETS_ENTITY_TYPE, entity: widget)

      let json = try JSON(data: entitiesCollection.encode().rawData())

      XCTAssertEqual(json["entities"][EntityKitTests.WIDGETS_ENTITY_TYPE][String(widget.id)]["id"].uIntValue, widget.id)
      XCTAssertEqual(json["entities"][EntityKitTests.WIDGETS_ENTITY_TYPE][String(widget.id)]["name"].stringValue, widget.name)
    } catch {
      XCTAssertTrue(false)
    }
  }

  func testDecode() {
    do {
      EntityRegistrar.registerDecodableEntityType(entityTypeKey: EntityKitTests.WIDGETS_ENTITY_TYPE, entityType: Widget.self)

      let widget = EntityKitTests.WIDGET1
      let json = try JSON(data: Data("""
      {
        "entities" : {
          "\(EntityKitTests.WIDGETS_ENTITY_TYPE)" : {
            "\(widget.id)" : {
              "id" : \(widget.id),
              "name" : "\(widget.name)"
            }
          }
        }
      }
      """.utf8))
      let entitiesCollection = EntitiesCollection.decode(json: json)
      let expectedEntitiesCollection = EntitiesCollection()
      expectedEntitiesCollection.unsafePutEntity(entityType: EntityKitTests.WIDGETS_ENTITY_TYPE, entity: widget)

      XCTAssertEqual(entitiesCollection.count, 1)
      XCTAssertEqual(
        entitiesCollection,
        expectedEntitiesCollection
      )
    } catch {
      XCTAssertTrue(false)
    }
  }
}
