import SwiftyJSON


protocol DecodableEntity {
  static func decode(json: JSON) -> Entity
}
