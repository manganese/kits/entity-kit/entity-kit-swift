import SwiftyJSON


protocol DecodableEntitiesCollection {
  associatedtype EntitiesCollectionType

  static func decode(json: JSON) -> EntitiesCollectionType
}
