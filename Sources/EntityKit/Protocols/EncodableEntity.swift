import SwiftyJSON


protocol EncodableEntity {
  func encode() -> JSON
}
