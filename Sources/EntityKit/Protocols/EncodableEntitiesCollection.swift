import SwiftyJSON


protocol EncodableEntitiesCollection {
  func encode() -> JSON
}
