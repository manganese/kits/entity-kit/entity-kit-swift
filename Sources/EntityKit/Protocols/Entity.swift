import SwiftyJSON


protocol Entity {
  var id: UInt { get }

  func isEqualTo(_ other: Entity) -> Bool
}

extension Entity where Self: Equatable {
  func isEqualTo(_ other: Entity) -> Bool {
    guard let otherEntity = other as? Self else { return false }

    return self == otherEntity
  }
}
