struct EntityRegistrar {
  static var decodableEntityTypes = [String: DecodableEntity.Type]()

  static func registerDecodableEntityType(entityTypeKey: String, entityType: DecodableEntity.Type) {
    decodableEntityTypes[entityTypeKey] = entityType
  }

  static func getDecodableEntityType(entityTypeKey: String) -> DecodableEntity.Type? {
    return decodableEntityTypes[entityTypeKey]
  }
}
