import Foundation
import SwiftyJSON


/**
 A collection of entities referenced by entity type and ID.
*/
class EntitiesCollection: EncodableEntitiesCollection, DecodableEntitiesCollection, Sequence, Equatable {
  // MARK: - Private state
  private var entities = [String: [UInt: Entity]]()

  // MARK: - Constructors
  /**
   Create a new, empty entities collection.

   Example:

   ```swift
   let entities = EntitiesCollection()
   ```

   - CorrelationId: constructor
  */
  init() {}

  /**
   Create a new entities collection and copy the data from an existing instance.

   Example:

   ```swift
   let entities1 = EntitiesCollection().putEntity("widgets", widget1)
   let entities2 = EntitiesCollection(from: entities1)
   entities2.getEntity("widgets", 1) // widget1
   ```

   - Parameter entitiesCollection: The entities collection from which to copy data.
   - CorrelationId: constructor-from
  */
  init(from entitiesCollection: EntitiesCollection) {
    for (entityType, entity) in entitiesCollection {
      self.unsafePutEntity(entityType: entityType, entity: entity)
    }
  }

  // MARK: - Unsafe
  /**
   Upsert an entity of a given type in the entities collection.

   Example:

   ```swift
   let entities = EntitiesCollection()
   entities.unsafePutEntity("widgets", widget1)
   entities.getEntity("widgets", 1) // widget1
   ```

   - Warning: The internal entities data structure is modified.
   - Parameter entityType: The entity type of the entity to put.
   - Parameter entity: The entity to put.
   - CorrelationId: unsafe-put-entity
  */
  func unsafePutEntity(entityType: String, entity: Entity) {
    if self.entities[entityType] == nil {
      self.entities[entityType] = [UInt: Entity]()
    }

    var entitiesOfType = self.entities[entityType]
    entitiesOfType?[entity.id] = entity

    self.entities[entityType] = entitiesOfType
  }

  /**
   Remove an entity of a given type by its ID from the entities collection.

   Example:

   ```swift
   let entities = EntitiesCollection().putEntity("widgets", widget1)
   entities.getEntity("widgets", 1) // widget1
   entities.unsafeRemoveEntity("widgets", 1)
   entities.getEntity("widgets", 1) // nil
   ```

   - Warning: The internal entities data structure is modified.
   - Parameter entityType: The entity type of the entity to remove.
   - Parameter id: The ID of the entity to remove.
   - CorrelationId: unsafe-remove-entity
  */
  func unsafeRemoveEntity(entityType: String, id: UInt) {
    var entitiesOfType = self.entities[entityType]

    if entitiesOfType == nil {
      return
    }

    entitiesOfType?.removeValue(forKey: id)

    self.entities[entityType] = entitiesOfType
  }

  // MARK: - Access
  /**
   Get the types of entities contained in this entities collection.

   Example:

   ```swift
   let entities =
     EntitiesCollection()
     .putEntity("widgets", widget)
     .putEntity("thingies", thingy)
   entities.getEntityTypes() // Set[ "widgets", "thingies" ]
   ```
  */
  func getEntityTypes() -> Set<String> {
    return Set(self.entities.keys)
  }

  /**
   Check if an entity of a given type with the given ID is present in the entities collection.

   - Parameter entityType: The entity type of the entity to check for.
   - Parameter id: The ID of the entity to check for.
  */
  func hasEntity(entityType: String, id: UInt) -> Bool {
    return self.getEntity(entityType: entityType, id: id) != nil
  }

  /**
   Get a single entity of a given entity type by its ID from the entities collection.

   - Parameter entityType: The entity type of the entity to get.
   - Parameter id: The ID of the entity to get.
  */
  func getEntity(entityType: String, id: UInt) -> Entity? {
    if let entitiesOfType = self.entities[entityType] {
      if let entity = entitiesOfType[id] {
        return entity
      }
    }

    return nil
  }

  /**
   Get all entities of a given entity type from the entities collection.

   - Parameter entityType: The entity type of the entities to get.
  */
  func getEntitiesOfType(entityType: String) -> [UInt: Entity] {
    if let entitiesOfType = self.entities[entityType] {
      return entitiesOfType
    }

    return [UInt: Entity]()
  }

  // MARK: - Functional Mutation
  /**
   Put a single entity of a given entity type into a copy of the entities collection.

   - Parameter entityType: The entity type of the entity to put.
   - Parameter entity: The entity to put.
  */
  func putEntity(entityType: String, entity: Entity) -> EntitiesCollection {
    let entitiesCollection = EntitiesCollection(from: self)
    entitiesCollection.unsafePutEntity(entityType: entityType, entity: entity)

    return entitiesCollection
  }

  /**
   Put multiple entities of a given entity type into a copy of the entities collection.

   - Parameter entityType: The entity type of the entity to put.
   - Parameter entitiesOfType: The entities to put.
  */
  func putEntity(entityType: String, entitiesOfType: [Entity]) -> EntitiesCollection {
    let entitiesCollection = EntitiesCollection(from: self)

    for entity in entitiesOfType {
      entitiesCollection.unsafePutEntity(entityType: entityType, entity: entity)
    }

    return entitiesCollection
  }

  /**
   Put entities of multiple entity types into a copy of the entities collection.

   - Parameter entities: A dictionary of entity types to sets of entities to put.
  */
  func putEntities(entities: [String: [Entity]]) -> EntitiesCollection {
    let entitiesCollection = EntitiesCollection(from: self)

    for (entityType, entitiesOfType) in entities {
      for entity in entitiesOfType {
        entitiesCollection.unsafePutEntity(entityType: entityType, entity: entity)
      }
    }

    return entitiesCollection
  }

  /**
   Put a single entity of a given entity type into a copy of the entities collection.

   - Parameter entityType: The entity type of the entity to remove.
   - Parameter id: The ID of the entity to remove.
  */
  func putEntity(entityType: String, id: UInt) -> EntitiesCollection {
    let entitiesCollection = EntitiesCollection(from: self)
    entitiesCollection.unsafeRemoveEntity(entityType: entityType, id: id)

    return entitiesCollection
  }

  // MARK: - Miscellaneous
  /**
   Determines whether at least one entity of any type is contained in the entities collection.
  */
  var empty: Bool {
    get {
      for _ in self {
        return false
      }

      return true
    }
  }

  /**
   Determines the total number of unique entities contained in the entities collection.
  */
  var count: UInt {
    get {
      var count: UInt = 0

      for _ in self {
        count += 1
      }

      return count
    }
  }

  // MARK: - Serialization
  func encode() -> JSON {
    var encodedEntities = [String: [String: JSON]]()

    for (entityType, entitiesOfType) in self.entities {
      var encodedEntitiesOfType = [String: JSON]()

      for (id, entity) in entitiesOfType {
        if entity is EncodableEntity {
          let encodedEntity = (entity as! EncodableEntity).encode()

          encodedEntitiesOfType[String(id)] = encodedEntity
        }
      }

      encodedEntities[entityType] = encodedEntitiesOfType
    }

    return [
      "entities": encodedEntities
    ]
  }

  // MARK: - Deserialization
  static func decode(json: JSON) -> EntitiesCollection {
    let entitiesCollection = EntitiesCollection()

    for (entityType, encodedEntitiesOfTypeJson) in json["entities"].dictionaryValue {
      let encodedEntitiesOfType = encodedEntitiesOfTypeJson.dictionaryValue

      for (_, encodedEntity) in encodedEntitiesOfType {
        let decodableEntityType = EntityRegistrar.getDecodableEntityType(entityTypeKey: entityType)
        let entity = decodableEntityType?.decode(json: encodedEntity)

        entitiesCollection.unsafePutEntity(entityType: entityType, entity: entity!)
      }
    }

    return entitiesCollection
  }

  // MARK: - Sequence
  func makeIterator() -> IndexingIterator<Array<(String, Entity)>> {
    var entitiesWithType: [(String, Entity)] = []

    for (entityType, entitiesOfType) in self.entities {
      for (_, entity) in entitiesOfType {
        entitiesWithType.append((entityType, entity))
      }
    }

    return entitiesWithType.makeIterator();
  }

  // MARK: - Equatable
  static func == (entitiesCollection1: EntitiesCollection, entitiesCollection2: EntitiesCollection) -> Bool {
    if entitiesCollection1.count != entitiesCollection2.count {
      return false
    }

    for (entityType, entity1) in entitiesCollection1 {
      let id = entity1.id

      if let entity2 = entitiesCollection2.getEntity(entityType: entityType, id: id) {
        if !entity1.isEqualTo(entity2) {
          return false
        }
      } else {
        return false
      }
    }

    return true
  }
}
